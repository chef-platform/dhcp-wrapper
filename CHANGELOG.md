Changelog
=========

1.4.0
-----

Main:

- massive clean-up following chef-platform standards
- chore: handover maintenance to make.org

Tests:

- test: start Continuous Integration with gitlab-ci
  + test: include .gitlab-ci.yml from test-cookbook
- test: add a correct Gemfile
- test: set seccomp (for systemd)
- test: use kitchen\_command.rb from test-cookbook
- test: replace deprecated require\_chef\_omnibus

Misc:

- chore: set new contributing guide with karma style
- chore: add foodcritic & rubocop in Gemfile
- doc: fix markdown in changelog and rename to CHANGELOG.md
- style(rubocop): fix delimiter offenses
- style: use cookbook\_name macro everywhere

1.3.0
-----

Main:

- Allow clients and servers to share the same role with specific options
  + Add an init recipe which initialize the configuration
  + Modify default to choose between client and server recipe
  + Call network\_interface in client and server recipes
  + client specific configurations should be in 'client-config' and
    servers in 'server-config'.
  + the servers should be defined with 'servers' key
- Rename network-interface to network\_interface: breaking change for users
  using it directly (very unlikely)
- Switch to docker\_cli, use prepared docker image
  + Switch kitchen driver from docker to docker\_cli
  + Use sbernard/centos-systemd-kitchen image instead of bare centos
  + Remove many monkey patches
  + No more need to have sudo access anymore: pipework is gone :)
  + Remove privileged mode :), use --cap-add=NET\_ADMIN instead
  + create a docker network: kitchen-dhcp, which is to be controled by
    the dhcp server

Misc:

- Fix and improve tests
  + Modify test cases to test specific option behavior
  + Latest dhcp and strengthen ip check
- Improve doc, explain specific conf and update it
- Fix all rubocop offenses

1.2.0
-----

- Fix idempotency by removing the block hack in network-interface recipe
- Reorganize README:
  + Move changelog from README to CHANGELOG
  + Move contribution guide to CONTRIBUTING.md
  + Reorder README, fix Gemfile missing
- Add Apache 2 license file

1.1.0
-----

- Add support for static routes

1.0.0
-----

- Initial version with Centos 7 support
