name 'dhcp-wrapper'
maintainer 'Chef Platform'
maintainer_email 'incoming+chef-platform/dhcp-wrapper@incoming.gitlab.com'
license 'Apache-2.0'
description 'Wrapper around dhcp cookbook'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
source_url 'https://gitlab.com/chef-platform/dhcp-wrapper'
issues_url 'https://gitlab.com/chef-platform/dhcp-wrapper/issues'
version '1.5.0'

supports 'centos', '>= 7.1'

chef_version '>= 12.19'

depends 'dhcp'
depends 'network_interfaces_v2'
